# Todo Application

## Overview

This project is a simple web-based Todo application that allows users to manage their tasks. Users can add new tasks, mark tasks as complete, and delete tasks. The application has a clean and intuitive user interface, making it easy for users to interact with their todo list.

## Features

- **Add Tasks**: Users can add new tasks to their todo list.
- **Mark Tasks as Complete**: Users can toggle the completion status of tasks to mark them as complete or incomplete.
- **Delete Tasks**: Users can remove tasks from their todo list.
- **Persistent Storage**: Tasks are stored in a SQLite database, ensuring that they persist between application restarts.

## Technologies Used

- **Flask**: A micro web framework written in Python used to build the web application.
- **Flask-SQLAlchemy**: An extension for Flask that adds support for SQLAlchemy, which is an Object Relational Mapper (ORM) for database interactions.
- **SQLite**: A lightweight disk-based database to store the todo items.
- **Semantic UI**: A front-end framework used for styling the application and making it responsive.
- **Jinja2**: A templating engine for Python, used to render the HTML templates with dynamic data.

## How to Use

To use the application, simply enter the title of a new task in the input field and click the "Add" button. The task will appear in the list below. You can mark a task as complete or incomplete by clicking the corresponding button, and you can delete a task by clicking the "Delete" button.

## Storage

The application uses SQLite as its storage mechanism. The database file `todo.db` is automatically created in the project's root directory. The `Todo` model defines the structure of the database table, which includes an `id` for the primary key, a `title` for the task description, and a `complete` boolean to indicate the task's completion status.

## Running the Application

To run the application, follow these steps:

1. Ensure you have Python installed on your system.
2. Install the required dependencies by running `pip install flask flask_sqlalchemy`.
3. Navigate to the project directory in your terminal.
4. Run the application with the command `python app.py`.
5. Open a web browser and go to `http://0.0.0.0:5000/` to view the application.

Make sure to activate the virtual environment if you are using one before installing dependencies and running the application.
